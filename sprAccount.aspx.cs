﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CiDataLabTest
{
    public partial class sprAccount : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        protected void cbDep_SelectedIndexChanged(object sender, EventArgs e)
        {
            setFltr();
        }
        protected void setFltr()
        {
            DS.FilterExpression = cbDep.SelectedValue == DBNull.Value.ToString() ? null : "customer_id=" + cbDep.SelectedValue;
        }
        protected void gr_RowEditing(object sender, GridViewEditEventArgs e)
        {
            setFltr();
            gr.EditIndex = e.NewEditIndex;
        }
        protected void gr_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            setFltr();
        }

        protected void gr_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            DS.SelectParameters["isAdd"].DefaultValue = "0";
            gr.EditIndex = -1;
            setFltr();
        }

        protected void btnOk_Click(object sender, ImageClickEventArgs e)
        {
            ImageButton btn = sender as ImageButton;
            GridViewRow r = btn.Parent.Parent as GridViewRow;
            int idx = r.RowIndex;
            string accountid = gr.DataKeys[idx][1].ToString();
            string account = ((TextBox)r.FindControl("cbAccount")).Text;
            string name = ((TextBox)r.FindControl("cbName")).Text;
            string bik = ((TextBox)r.FindControl("cbBik")).Text;
            string balance = ((TextBox) r.FindControl("cbBalance")).Text;
            if (accountid == "0") //Добавление нового счета
            {
                string customer_id = ((DropDownList)r.FindControl("cbCustomerId")).SelectedValue;
                DS.InsertParameters["customer_id"].DefaultValue = customer_id;
                DS.InsertParameters["account"].DefaultValue = account;
                DS.InsertParameters["name"].DefaultValue = name;
                DS.InsertParameters["bik"].DefaultValue = bik;
                DS.InsertParameters["balance"].DefaultValue = balance.Replace('.', ',');
                DS.Insert();
            }
            else {
                DS.UpdateParameters["account"].DefaultValue = account;
                DS.UpdateParameters["name"].DefaultValue = name;
                DS.UpdateParameters["bik"].DefaultValue = bik;
                DS.UpdateParameters["balance"].DefaultValue = balance.Replace('.', ',');
                DS.UpdateParameters["account_no"].DefaultValue = accountid;
                DS.Update();
            }
            setFltr();
            DS.SelectParameters["isAdd"].DefaultValue = "0";
            DS.DataBind();
            gr.DataBind();
            gr.EditIndex = -1;

        }

        protected void BtnAdd_Click(object sender, ImageClickEventArgs e)
        {
            DS.SelectParameters["isAdd"].DefaultValue = "1";
            gr.DataBind();
            gr.EditIndex = 0;
        }
    }
}