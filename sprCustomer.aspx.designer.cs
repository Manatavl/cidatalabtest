﻿//------------------------------------------------------------------------------
// <автоматически создаваемое>
//     Этот код создан программой.
//
//     Изменения в этом файле могут привести к неправильной работе и будут потеряны в случае
//     повторной генерации кода. 
// </автоматически создаваемое>
//------------------------------------------------------------------------------

namespace CiDataLabTest
{


    public partial class sprCustomer
    {

        /// <summary>
        /// lHeader элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически созданное поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label lHeader;

        /// <summary>
        /// cbFltrName элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически созданное поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox cbFltrName;

        /// <summary>
        /// cbFltrName_AutoCompleteExtender элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически созданное поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::AjaxControlToolkit.AutoCompleteExtender cbFltrName_AutoCompleteExtender;

        /// <summary>
        /// BtnFind элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически созданное поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::System.Web.UI.WebControls.ImageButton BtnFind;

        /// <summary>
        /// BtnAdd элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически созданное поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::System.Web.UI.WebControls.ImageButton BtnAdd;

        /// <summary>
        /// DS элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически созданное поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::System.Web.UI.WebControls.SqlDataSource DS;

        /// <summary>
        /// up элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически созданное поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::System.Web.UI.UpdatePanel up;

        /// <summary>
        /// gr элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически созданное поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::System.Web.UI.WebControls.GridView gr;

        /// <summary>
        /// pnlAdd элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически созданное поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Panel pnlAdd;

        /// <summary>
        /// updPnlNew элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически созданное поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::System.Web.UI.UpdatePanel updPnlNew;

        /// <summary>
        /// Label16 элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически созданное поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label Label16;

        /// <summary>
        /// cbNameNew элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически созданное поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox cbNameNew;

        /// <summary>
        /// RequiredFieldValidator2 элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически созданное поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::System.Web.UI.WebControls.RequiredFieldValidator RequiredFieldValidator2;

        /// <summary>
        /// Label4 элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически созданное поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label Label4;

        /// <summary>
        /// cbINNNew элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически созданное поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox cbINNNew;

        /// <summary>
        /// lblInndErr элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически созданное поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label lblInndErr;

        /// <summary>
        /// Label5 элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически созданное поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label Label5;

        /// <summary>
        /// cbAddressAdd элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически созданное поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox cbAddressAdd;

        /// <summary>
        /// lblAddErr элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически созданное поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label lblAddErr;

        /// <summary>
        /// btnAddNew элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически созданное поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::System.Web.UI.WebControls.ImageButton btnAddNew;

        /// <summary>
        /// btnNoAddNew элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически созданное поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::System.Web.UI.WebControls.ImageButton btnNoAddNew;
    }
}
