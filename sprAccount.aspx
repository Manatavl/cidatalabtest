﻿<%@ Page Title="sprAccount" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="sprAccount.aspx.cs" Inherits="CiDataLabTest.sprAccount" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <style type="text/css">

    </style>
    <script language="javascript" type="text/javascript">
        function clientValidate(sender, args) {
            if (args.Value.length > 0 && args.Value.length != 20) {
                args.IsValid = false;
            }
        }
        function clientValidateBIK(sender, args) {
            if (args.Value.length > 0 && args.Value.length != 9) {
                args.IsValid = false;
            }
        }
        function clientValidateBalance(sender, args) {
            let err = '';
            if (args.Value.length == 0) {
                err = 'Обязательное поле';
            }
            if (Number(args.Value) > 999999999999.99) err = 'Превышено допустимое значение';
            if (err != '') {
                sender.textContent = err;
                args.IsValid = false;
            }
        }
        function OnKey(num, obj) {
            console.log('obj=', obj.value);
            if ((num >= 48 && num <= 57) | num == 46 | num == 44)
            {
                var str = obj.value.toString().replace(",", ".");
                if (str.indexOf('.') >= 0 ) {
                    let pos = str.split('.')[1].length;
                    if (pos >= 2) return false;
                }
               return true;
            }
            else return false;
           
        }
    </script>
    <main aria-labelledby="title">
        <div id="pnlFltr" style="display:flex">
            <asp:SqlDataSource ID="DS_Dep" runat="server" ConnectionString="<%$ ConnectionStrings:sqlConnect %>" 
                SelectCommand="select null as id, null as name
union all
SELECT id, name FROM  Customer
order by name asc -- implies nulls first"></asp:SqlDataSource>
            <asp:Label ID="lHeader" runat="server" Font-Bold="False" Font-Size="X-Large" ForeColor="#000099" Text="Счета Организаций" ></asp:Label>
            <div style="margin-left:30px;">
                    <asp:DropDownList ID="cbDep" runat="server" Width="350px" DataSourceID="DS_Dep" DataTextField="NAME" DataValueField="ID" AutoPostBack="True" OnSelectedIndexChanged="cbDep_SelectedIndexChanged"></asp:DropDownList>
            </div>
            <div style="margin-left:30px;">
                <asp:ImageButton ID="BtnAdd" runat="server" cssClass= "buttonstyle" ImageUrl="~/Image/add.png" OnClick="BtnAdd_Click" ToolTip="Добавить организацию" />  
            </div>
        </div>
        <div>
            <asp:SqlDataSource ID="DS" runat="server" ConnectionString="<%$ ConnectionStrings:sqlConnect %>"
                SelectCommand="select null as customer_id, null as customer, null as inn, null as address, 0 as account_no, null as account, null as name, null as  bik, 0 as balance
where 1 = @isAdd
union all
SELECT customer.id as customer_id, Customer.name as customer, inn, address,  Account.account_no, Account.account, Account.name, bik, balance
FROM  Customer join Account on Customer.id = Account.customer_id
order by Customer, account -- implies nulls first
" DeleteCommand="delete from Account where account_no = @account_no" InsertCommand="insert into Account (customer_id, account, name, bik, balance) values (@customer_id, @account, @name, @bik, @balance)" 
                UpdateCommand="update Account set 
  account = @account, 
  name = @name, 
  bik = @bik, 
  balance = @balance
where account_no = @account_no">
                <DeleteParameters>
                    <asp:Parameter DbType="Int32" Name="account_no" />
                </DeleteParameters>
                <InsertParameters>
                    <asp:Parameter DbType="Int32" Name="customer_id" />
                    <asp:Parameter DbType="String" Name="account" />
                    <asp:Parameter DbType="String" Name="name" />
                    <asp:Parameter DbType="String" Name="bik" />
                    <asp:Parameter DbType="Double" Name="balance" />
                </InsertParameters>
                <SelectParameters>
                    <asp:Parameter DbType="Int32" DefaultValue="0" Name="isAdd" />
                </SelectParameters>
                <UpdateParameters>
                    <asp:Parameter DbType="String" Name="account" />
                    <asp:Parameter DbType="String" Name="name" />
                    <asp:Parameter DbType="String" Name="bik" />
                    <asp:Parameter DbType="Double" Name="balance" />
                    <asp:Parameter DbType="Int32" Name="account_no" />
                </UpdateParameters>
            </asp:SqlDataSource>
            <asp:SqlDataSource ID="DS_SelDep" runat="server" ConnectionString="<%$ ConnectionStrings:sqlConnect %>" 
                SelectCommand="SELECT id, name FROM  Customer
order by name asc -- implies nulls first"></asp:SqlDataSource>
            <asp:GridView ID="gr" runat="server" AutoGenerateColumns="False" BorderStyle="Solid" BorderWidth="1px" CellPadding="4" DataKeyNames="CUSTOMER_ID,ACCOUNT_NO" DataSourceID="DS" ForeColor="#333333" OnRowCancelingEdit="gr_RowCancelingEdit" OnRowDeleting="gr_RowDeleting" OnRowEditing="gr_RowEditing" EmptyDataText="Счета не найдены">
                <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                <Columns>
                    <asp:TemplateField HeaderText="Организация">
                        <EditItemTemplate>
                            <asp:Label ID="Label1" runat="server" Text='<%# Eval("CUSTOMER") %>' Visible='<%# Eval("CUSTOMER_ID") != DBNull.Value %>'></asp:Label>
                            <asp:DropDownList ID="cbCustomerId" runat="server" DataSourceID="DS_SelDep" DataTextField="NAME" DataValueField="ID" Visible='<%# Eval("CUSTOMER_ID") == DBNull.Value %>'>
                            </asp:DropDownList>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label5" runat="server" Text='<%# Bind("CUSTOMER") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle Width="300px" />
                    </asp:TemplateField>
                    <asp:BoundField DataField="INN" HeaderText="ИНН" ReadOnly="True">
                    <ItemStyle HorizontalAlign="Left" Width="70px" />
                    </asp:BoundField>
                    <asp:BoundField DataField="ADDRESS" HeaderText="Адрес" ReadOnly="True">
                    <ItemStyle HorizontalAlign="Left" Width="300px" />
                    </asp:BoundField>
                    <asp:TemplateField HeaderText="Счет">
                        <EditItemTemplate>
                            <asp:TextBox ID="cbAccount" runat="server" Text='<%# Bind("ACCOUNT") %>' Width="190px" MaxLength="20" ValidationGroup="grErr"></asp:TextBox>
                            <asp:MaskedEditExtender ID="cbAccount_MaskedExt" runat="server" BehaviorID="cbAccount_MaskedEditExtender" Century="2000" CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat="" CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder="" CultureTimePlaceholder="" Mask="99999999999999999999" TargetControlID="cbAccount" ErrorTooltipEnabled="True" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="cbAccount" ErrorMessage="Обязательное поле" ForeColor="Red" ValidationGroup="grErr"></asp:RequiredFieldValidator>
                            <asp:CustomValidator runat="server" ID="CustomValidator1" ControlToValidate="cbAccount" ClientValidationFunction="clientValidate" Display="Dynamic" ErrorMessage="Длина меньше 20" ForeColor="Red" ValidationGroup="grErr"></asp:CustomValidator>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label1" runat="server" Text='<%# Bind("ACCOUNT") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" Width="150px" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Название счета">
                        <EditItemTemplate>
                            <asp:TextBox ID="cbName" runat="server" Text='<%# Bind("NAME") %>' MaxLength="100" Rows="3" TextMode="MultiLine" Width="300px"></asp:TextBox>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label2" runat="server" Text='<%# Bind("NAME") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" Width="250px" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="БИК банка">
                        <EditItemTemplate>
                            <asp:TextBox ID="cbBik" runat="server" Text='<%# Bind("BIK") %>' MaxLength="9"></asp:TextBox>
                            <asp:MaskedEditExtender ID="cbBik_MaskedEditExt" runat="server" BehaviorID="cbBik_MaskedEditExtender" Century="2000" CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat="" CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder="" CultureTimePlaceholder="" Mask="999999999" TargetControlID="cbBik" ErrorTooltipEnabled="True"/>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="cbBik" ErrorMessage="Обязательное поле" ForeColor="Red" ValidationGroup="grErr"></asp:RequiredFieldValidator>
                            <br />
                            <asp:CustomValidator ID="CustomValidator2" runat="server" ClientValidationFunction="clientValidateBIK" Display="Dynamic" 
                                ErrorMessage="БИК меньше 9 символов" ForeColor="Red" ControlToValidate="cbBik" CssClass="mt" ValidationGroup="grErr"></asp:CustomValidator>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label3" runat="server" Text='<%# Bind("BIK") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" Width="50px" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Остаток на счете">
                        <EditItemTemplate>
                            <asp:TextBox ID="cbBalance" runat="server" MaxLength="15" Text='<%# Bind("BALANCE") %>' onkeypress="return OnKey(event.keyCode,this)"></asp:TextBox>
                            <asp:CustomValidator ID="CustomValidatorBalance" runat="server" ClientValidationFunction="clientValidateBalance" ControlToValidate="cbBalance" Display="Dynamic" ErrorMessage="Ошибка" ForeColor="Red" ValidationGroup="grErr"></asp:CustomValidator>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="cbBalance" Display="Dynamic" ErrorMessage="Обязательное поле" ForeColor="Red" ValidationGroup="grErr"></asp:RequiredFieldValidator>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label4" runat="server" Text='<%# Bind("BALANCE", "{0:F2}") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" Width="70px" />
                    </asp:TemplateField>
                    <asp:TemplateField ShowHeader="False">
                        <EditItemTemplate>
                            <asp:ImageButton ID="btnOk" runat="server" CausesValidation="True" Height="20px" ImageUrl="~/Image/ok.png" Text="Обновить" ValidationGroup="grErr" OnClick="btnOk_Click" />
                            &nbsp;<asp:ImageButton ID="ImageButton2" runat="server" CausesValidation="False" CommandName="Cancel" Height="20px" ImageUrl="~/Image/cansel.png" Text="Отмена" />
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:ImageButton ID="ImageButton1" runat="server" CausesValidation="False" CommandName="Edit" Height="20px" ImageUrl="~/Image/edit.png" Text="Правка" />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField ShowHeader="False">
                        <ItemTemplate>
                            <asp:ImageButton ID="btnDel" runat="server" CausesValidation="False" CommandName="Delete" Height="20px" ImageUrl="~/Image/delete.png" Text="Удалить" ToolTip="Удалить счет" />
                            <asp:ConfirmButtonExtender ID="btnDel_ConfirmButtonExtender" runat="server" BehaviorID="btnDel_ConfirmButtonExtender" ConfirmText="Подтвердить удаление счета?" TargetControlID="btnDel" />
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <EditRowStyle BackColor="#999999" />
                <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" BorderStyle="Solid" BorderWidth="1px" HorizontalAlign="Center" />
                <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                <RowStyle BackColor="#F7F6F3" ForeColor="#333333" BorderStyle="Solid" BorderWidth="1px" VerticalAlign="Top" />
                <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                <SortedAscendingCellStyle BackColor="#E9E7E2" />
                <SortedAscendingHeaderStyle BackColor="#506C8C" />
                <SortedDescendingCellStyle BackColor="#FFFDF8" />
                <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
            </asp:GridView>
        </div>

    </main>
</asp:Content>

