﻿<%@ Page Title="sprCustomer" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="sprCustomer.aspx.cs" Inherits="CiDataLabTest.sprCustomer" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <style type="text/css">
        .style1 {
            width:2px;
        }
        .style3 {
            width:500px;
        }
        .style4 {
            margin-left:2px;
            margin-right:2px;
            width:98%;
        }
        style5 {
            width:70%;
        }
        .styleError {
            background-color: lightpink;
        }
        .styleOk {
            background-color: inherit;
        }
        .style7 {
        text-align: center; 
        font-size: small; 
        color: Navy; 
        font-weight: bold;
        }
        .edittxt {
            max-width:500px;
            width:500px;
            margin-left: 10px;
        }
        .buttonstyle {
            height:32px;
            width:32px;
            margin-left:20px;
        }
        .PanelPopup {
            background-color:gainsboro;
            position: absolute;
            top: 50%;
            left: 50%;
            transform: translateX(-50%) translateY(-50%);
        }
        
    </style> 
    <script language="javascript" type="text/javascript">
        function SetZIndex(control, args) {
            control._completionListElement.style.zIndex = 99999999;
        }
        function showCustomer() {
            var btn = document.getElementById('<%= BtnFind.ClientID %>');
            if (btn) btn.click();
        }
        function OnKey(num, obj) {
            if (num >= 48 && num <= 57) return true;
            else return false;
        }
        function pnlNoVisible() {
            var pnl = document.getElementById('<%= pnlAdd.ClientID %>');
            if (pnl) pnl.style.display = 'none';
            var cb = document.getElementById('<%= cbNameNew.ClientID %>');
            if (cb) 
                cb.value = '';
            
            cb = document.getElementById('<%= cbINNNew.ClientID %>');
            if (cb) 
                cb.value = '';
            
            cb = document.getElementById('<%= cbAddressAdd.ClientID %>');
            if (cb) 
                cb.value = '';
            
        }
        function pnlVisible() {
            var pnl = document.getElementById('<%= pnlAdd.ClientID %>');
            pnl.style.display = '';
        }
        function doBackServer(obj, par) {  // doBackServer('PnlSetViewUsers', true);
            var ObjId = obj;
            __doPostBack(ObjId, par);
        }
    </script>
    <main aria-labelledby="title">
        <div id="pnlFltr" style="display:flex">
            <asp:Label ID="lHeader" runat="server" Font-Bold="False" Font-Size="X-Large" ForeColor="#000099" Text="Справочник организаций" ></asp:Label>
            <div style="margin-left:30px;">
                <div>
                    <asp:TextBox ID="cbFltrName" runat="server" Width="350px"></asp:TextBox>
                <asp:AutoCompleteExtender ID="cbFltrName_AutoCompleteExtender" runat="server" DelimiterCharacters="" Enabled="True" 
                    OnClientShown ="SetZIndex" ServiceMethod="GetCustomer" ServicePath="ServiceProgect.asmx" 
                    TargetControlID="cbFltrName" OnClientItemSelected="showCustomer">
                </asp:AutoCompleteExtender>
                </div>
                <span class="style7">Наименование организации</span>
            </div>
            <asp:ImageButton ID="BtnFind" runat="server" cssClass= "buttonstyle" ImageUrl="~/Image/search.png" OnClick="BtnFind_Click" ToolTip="Найти" />
            <asp:ImageButton ID="BtnAdd" runat="server" cssClass= "buttonstyle" ImageUrl="~/Image/add.png" OnClick="BtnAdd_Click" ToolTip="Добавить организацию" OnClientClick="pnlVisible();return false;" />  
        </div>
        <asp:SqlDataSource ID="DS" runat="server" ConnectionString="<%$ ConnectionStrings:sqlConnect %>" DeleteCommand="delete from dbo.Customer where id = @id" ProviderName="System.Data.SqlClient" SelectCommand="select id, name, inn, address,
(select count (*) from dbo.Account where dbo.Account.customer_id = dbo.Customer.id) as k from dbo.Customer order by name" UpdateCommand="update dbo.Customer
set name = @name,
  inn = @inn,
  address = @address
where id = @id" InsertCommand="insert into [dbo].[Customer] (name, inn, address) values (@name, @inn, @address)
">
            <DeleteParameters>
                <asp:Parameter DbType="Int32" Name="id" />
            </DeleteParameters>
            <InsertParameters>
                <asp:ControlParameter ControlID="cbNameNew" DbType="String" Name="name" PropertyName="Text" />
                <asp:ControlParameter ControlID="cbINNNew" DbType="String" Name="inn" PropertyName="Text" />
                <asp:ControlParameter ControlID="cbAddressAdd" DbType="String" Name="address" PropertyName="Text" />
            </InsertParameters>
            <UpdateParameters>
                <asp:Parameter DbType="String" Name="name" />
                <asp:Parameter DbType="String" Name="inn" />
                <asp:Parameter DbType="String" Name="address" />
                <asp:Parameter DbType="Int32" Name="id" />
            </UpdateParameters>
        </asp:SqlDataSource>
            <asp:UpdatePanel ID="up" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <asp:GridView ID="gr" runat="server" DataSourceID="DS" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="ID" ForeColor="#333333" OnRowCancelingEdit="gr_RowCancelingEdit" OnRowEditing="gr_RowEditing" OnRowUpdating="gr_RowUpdating" BorderStyle="Solid" BorderWidth="1px">
                        <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                        <Columns>
                            <asp:TemplateField HeaderText="Наименование организации" >
                                <EditItemTemplate>
                                    <asp:TextBox ID="cbName" runat="server" Text='<%# Bind("NAME") %>' class="style3"></asp:TextBox>
                                    <div>
                                        <asp:CustomValidator ID="ValidatorName" runat="server" ClientValidationFunction="MyFunction" EnableClientScript="False" ErrorMessage="Превышена допустимая длина 100 символов" ForeColor="Red" OnServerValidate="ValidatorName_ServerValidate" 
                        ValidationGroup="grErr" ControlToValidate="cbName">Длина наименования превышает 100 символов</asp:CustomValidator>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Обязательное поле" ControlToValidate="cbName" ForeColor="Red" ValidationGroup="grErr">Обязательное поле</asp:RequiredFieldValidator>
                                    </div>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="Label1" runat="server" Text='<%# Bind("NAME") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="ИНН">
                                <EditItemTemplate>
                                    <asp:TextBox ID="cbINN" runat="server" Text='<%# Bind("INN") %>' onkeypress="return OnKey(event.keyCode,this)"
                        CausesValidation="True" MaxLength="12" ValidationGroup="grErr"></asp:TextBox>
                                    <br />
                                    <asp:CustomValidator ID="ValidatorINN" runat="server" ClientValidationFunction="MyFunction" ControlToValidate="cbINN" EnableClientScript="False" ErrorMessage="Длина ИНН меньше 12 символов" ForeColor="Red" OnServerValidate="ValidatorINN_ServerValidate" ValidationGroup="grErr">ИНН меньше 12 символов</asp:CustomValidator>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="Label2" runat="server" Text='<%# Bind("INN") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Адрес">
                                <EditItemTemplate>
                                    <asp:TextBox ID="cbAddress" runat="server" Text='<%# Bind("ADDRESS") %>' CssClass="style4" Rows="3" TextMode="MultiLine"></asp:TextBox>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="Label3" runat="server" Text='<%# Bind("ADDRESS") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:TemplateField>
                            <asp:TemplateField ShowHeader="False">
                                <EditItemTemplate>
                                    <asp:ImageButton ID="btnOk" runat="server" CausesValidation="True" CommandName="Update" Height="20px" ImageUrl="~/Image/ok.png" Text="Обновить" ValidationGroup="grErr" />
                                    &nbsp;<asp:ImageButton ID="btnCansel" runat="server" CausesValidation="False" CommandName="Cancel" Height="20px" ImageUrl="~/Image/cansel.png" Text="Отмена" />
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:ImageButton ID="ImageButton2" runat="server" CausesValidation="False" CommandName="Edit" Height="20px" ImageUrl="~/Image/edit.png" Text="Правка" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField ShowHeader="False">
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnDel" runat="server" CausesValidation="False" CommandName="Delete" Height="20px" ImageUrl="~/Image/delete.png" Text="Удалить" ToolTip="Удалить запись" Visible='<%# Convert.ToInt32(Eval("K")) == 0 %>' />
                                    <asp:ConfirmButtonExtender ID="btnDel_ConfirmButtonExtender" runat="server" BehaviorID="btnDel_ConfirmButtonExtender" ConfirmText="Удалить запись?" TargetControlID="btnDel" />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <EditRowStyle BackColor="#999999" VerticalAlign="Top" />
                        <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                        <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                        <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                        <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                        <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                        <SortedAscendingCellStyle BackColor="#E9E7E2" />
                        <SortedAscendingHeaderStyle BackColor="#506C8C" />
                        <SortedDescendingCellStyle BackColor="#FFFDF8" />
                        <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
                    </asp:GridView>
                </ContentTemplate>
            </asp:UpdatePanel>
        <div style="margin-top: 30px;">
    </div>
    <asp:Panel ID="pnlAdd" runat="server" CssClass="PanelPopup" Width="657px" style="display:none"><%-- --%>
        <asp:UpdatePanel ID="updPnlNew" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <h4 style="color:darkblue;text-align:center;">Добавление новой организации</h4>
                <table style="margin:5px;">
                    <tr>
                        <td>
                            <asp:Label ID="Label16" runat="server" Text="Наименование:"></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="cbNameNew" runat="server" CausesValidation="True" ValidationGroup="grAdd" MaxLength="100" CssClass="edittxt"></asp:TextBox>                                        
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="cbNameNew" ErrorMessage="Обязательное поле" ForeColor="Red" ValidationGroup="grAdd" style="margin-left: 10px;">Обязательное поле</asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="Label4" runat="server" Text="ИНН:"></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="cbINNNew" runat="server" MaxLength="12" onkeypress="return OnKey(event.keyCode,this)" CssClass="edittxt" ></asp:TextBox>
                            <asp:Label ID="lblInndErr" runat="server" ForeColor="Red" Text="ИНН меньше 12 символов" Visible="False" style="margin-left: 10px;"></asp:Label>
                        </td>            
                     </tr>
                    <tr>
                        <td>
                            <asp:Label ID="Label5" runat="server" Text="Адрес:"></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="cbAddressAdd" runat="server" CssClass="edittxt"></asp:TextBox>
                        </td>            
                     </tr>
                </table>   
                <asp:Label ID="lblAddErr" runat="server" ForeColor="Red" Text="Добавление невозможно! Данная организация существует." Visible="False"></asp:Label>
                <div class="flex-row">
                    <asp:ImageButton ID="btnAddNew" runat="server" ImageUrl="~/Image/ok.png" OnClick="btnAddNew_Click" ValidationGroup="grAdd" ToolTip="Сохранить" CssClass="m-4" />
                    <asp:ImageButton ID="btnNoAddNew" runat="server" Height="30px" ImageUrl="~/Image/cansel.png" OnClientClick="pnlNoVisible()" Width="30px" ToolTip="Отменить" CssClass="m-4" />
                </div>    
            </ContentTemplate>
        </asp:UpdatePanel>                
   </asp:Panel>
    </main>
</asp:Content>

