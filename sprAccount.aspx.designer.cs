﻿//------------------------------------------------------------------------------
// <автоматически создаваемое>
//     Этот код создан программой.
//
//     Изменения в этом файле могут привести к неправильной работе и будут потеряны в случае
//     повторной генерации кода. 
// </автоматически создаваемое>
//------------------------------------------------------------------------------

namespace CiDataLabTest
{


    public partial class sprAccount
    {

        /// <summary>
        /// DS_Dep элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически созданное поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::System.Web.UI.WebControls.SqlDataSource DS_Dep;

        /// <summary>
        /// lHeader элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически созданное поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label lHeader;

        /// <summary>
        /// cbDep элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически созданное поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::System.Web.UI.WebControls.DropDownList cbDep;

        /// <summary>
        /// BtnAdd элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически созданное поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::System.Web.UI.WebControls.ImageButton BtnAdd;

        /// <summary>
        /// DS элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически созданное поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::System.Web.UI.WebControls.SqlDataSource DS;

        /// <summary>
        /// DS_SelDep элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически созданное поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::System.Web.UI.WebControls.SqlDataSource DS_SelDep;

        /// <summary>
        /// gr элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически созданное поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::System.Web.UI.WebControls.GridView gr;
    }
}
