﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Security.Policy;

namespace CiDataLabTest
{
    public partial class sprCustomer : System.Web.UI.Page
    {
        protected void BtnFind_Click(object sender, ImageClickEventArgs e)
        {
            setFltr();
        }

        protected void BtnAdd_Click(object sender, ImageClickEventArgs e)
        {
            setFltr();
        }

        protected void ValidatorName_ServerValidate(object source, ServerValidateEventArgs args)
        {
            if (args.Value.Length > 100)
            {
                args.IsValid = false;
                (source as CustomValidator).Text = "Длина наименования больше 100 символов";
            }
            else if (args.Value.Length == 0)
            {
                args.IsValid = false;
                (source as CustomValidator).Text = "Обязательное поле";
            }
            else
            {
                args.IsValid = true;
            }
        }        

        protected void gr_RowEditing(object sender, GridViewEditEventArgs e)
        {
            setFltr();
        }

        protected void gr_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            setFltr();
        }

        protected void gr_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            setFltr();
        }
        protected void setFltr()
        {
            DS.FilterExpression = cbFltrName.Text.Trim().Length > 0 ? @"NAME like '" + cbFltrName.Text.Replace("'", "''") + "%'" : null;
        }

        protected void ValidatorINN_ServerValidate(object source, ServerValidateEventArgs args)
        {
            if (args.Value.Length != 12)
            {
                args.IsValid = false;
            }
            else
            {
                args.IsValid = true;
            }
        }

        protected void btnAddNew_Click(object sender, ImageClickEventArgs e)
        {
            lblAddErr.Visible = false;
            lblInndErr.Visible = false;
            if (cbINNNew.Text.Length < 12)
            {
                lblInndErr.Visible = true;
                return;
            }
            try
            {
                DS.Insert();
                DS.FilterExpression = null;
                up.Update();
                ScriptManager.RegisterStartupScript(Page, this.GetType(), "ClosePanel", "pnlNoVisible()", true);
            }
            catch (Exception ex) {
                if (ex.Message.IndexOf("UNIQUE KEY") != -1)
                    lblAddErr.Visible = true;
            }
        }
    }
}