﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Configuration;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// Чтобы разрешить вызывать веб-службу из скрипта с помощью ASP.NET AJAX, раскомментируйте следующую строку. 
[System.Web.Script.Services.ScriptService]
public class ServiceProgect : System.Web.Services.WebService
{
    public ServiceProgect()
    {
        //Раскомментируйте следующую строку в случае использования сконструированных компонентов 
        //InitializeComponent(); 
    }

    [WebMethod]
    public string[] GetCustomer(string prefixText)
    {
        DataTable dt = new DataTable();
        using (SqlConnection SQLCon = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlConnect"].ConnectionString))
        {
            if (SQLCon.State == System.Data.ConnectionState.Closed) SQLCon.Open();
            string SQL = @"select name from dbo.Customer where upper(name) like '" + prefixText.ToUpper().Replace("'","''") + "%'" +
                " ORDER BY name";
            SqlCommand SQLcmd = new SqlCommand(SQL, SQLCon);
            SqlDataAdapter da = new SqlDataAdapter(SQLcmd);
            
            da.Fill(dt);

            SQLCon.Close();
            SQLcmd.Dispose();
            da.Dispose();
        }
        return dt.AsEnumerable()
            .Select(r => r.Field<string>("NAME")).ToArray();
    }
}
